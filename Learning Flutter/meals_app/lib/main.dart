import 'package:flutter/material.dart';
import 'package:meals_app/screens/category_meals.dart';
import 'package:meals_app/screens/meal_detail.dart';
import 'screens/categories_screen.dart';
import 'package:google_fonts/google_fonts.dart';
void main(){
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Meals App',
      theme: ThemeData(
        canvasColor: const Color.fromRGBO(255, 254, 229, 1),
        textTheme: ThemeData.light().textTheme.copyWith(
          bodyMedium: const TextStyle(color: Color.fromRGBO(20,51,51,1)),
          titleLarge: GoogleFonts.robotoCondensed(
            fontSize: 20,
            fontWeight: FontWeight.bold
          )
        ), colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.pink).copyWith(secondary: Colors.amber)
      ),
      //home: const CategoriesScreen(),
      initialRoute: '/',
      routes: {
        '/':(ctx) => const CategoriesScreen(),
        CategoryMealsScreen.routeName:(ctx) =>const CategoryMealsScreen(),
        MealDetails.routeName:(ctx) => const MealDetails(),
      },

    );
  }
}