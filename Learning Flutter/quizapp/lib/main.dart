import 'package:flutter/material.dart';
import 'quiz.dart';
import 'result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final _questions = const [
    {
      'questionText': 'What\'s your favorite color?',
      'answers': [
        {'text': 'Black', 'score': 10},
        {'text': 'Red', 'score': 5},
        {'text': 'Green', 'score': 3},
        {'text': 'White', 'score': 1}
      ]
    },
    {
      'questionText': 'What\'s your favorite Animal?',
      'answers': [
        {'text': 'Tiger', 'score': 5},
        {'text': 'Lion', 'score': 8},
        {'text': 'Dog', 'score': 3}
      ]
    },
    {
      'questionText': 'What\'s your favorite food?',
      'answers': [
        {'text': 'Pizza', 'score': 9},
        {'text': 'Salad', 'score': 2},
        {'text': 'Ice Cream', 'score': 5},
        {'text': 'Burger', 'score': 8},
        {'text': 'Taco', 'score': 6}
      ]
    }
  ];
  var _totalScore = 0;
  var _questionIndex = 0;
  void _anserQuestion(int score) {
    _totalScore += score;
    setState(() {
      _questionIndex += 1;
    });
    if (_questionIndex + 1 < _questions.length) {
      debugPrint("We still have more questions");
    }
    debugPrint("Question $_questionIndex is showing");
    debugPrint("Score $_totalScore");
  }

  void _resetQuiz() {
    setState(() {
      _totalScore = 0;
      _questionIndex = 0;
    });
  }

  @override
  build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Mamcos Adike Price"),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(_anserQuestion, _questions, _questionIndex)
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
