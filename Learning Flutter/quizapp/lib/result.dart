import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int totalScore;
  final VoidCallback reset;
  Result(this.totalScore, this.reset);

  String get resultPhrase {
    String resultText = "Over";
    if (totalScore <= 8) {
      resultText = "You are good";
    } else if (totalScore <= 12) {
      resultText = "Maybe likeable";
    } else if (totalScore <= 16) {
      resultText = "Maybe strange";
    } else {
      resultText = "Pretty Bad";
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            resultPhrase,
            style: const TextStyle(
              fontSize: 36,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          TextButton(onPressed: reset, child: Text("Restart Quiz"))
        ],
      ),
    );
  }
}
