import 'package:expense_moniter/widgets/chart.dart';
import 'package:expense_moniter/widgets/new_transaction.dart';
import 'package:expense_moniter/widgets/transaction_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'models/transaction.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Personal Expenses',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.purple,
        colorScheme: ColorScheme.fromSwatch(
            primarySwatch: Colors.purple,

        ).copyWith(secondary: Colors.amber),
        fontFamily: 'Quicksand',
        appBarTheme: AppBarTheme(
          toolbarTextStyle: ThemeData.light().textTheme.copyWith(
            titleLarge: const TextStyle(
              fontFamily: 'OpenSans',
              fontSize: 20,
              fontWeight: FontWeight.bold
            )
          ).bodyText2, titleTextStyle: ThemeData.light().textTheme.copyWith(
            titleLarge: const TextStyle(
              fontFamily: 'OpenSans',
              fontSize: 20,
              fontWeight: FontWeight.bold
            )
          ).headline6
          )
      ),
      home: const MyHomePage(),
    );
  }
}


class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final List<Transaction> _userTransactions=[
    // Transaction(id:'t1', title: 'New Shoes', amount: 59.99, date:DateTime.now()),
    // Transaction(id: 't2',title: 'weekly groceries', amount: 16.53, date:DateTime.now())
  ];
  bool _showChart = false;



  List<Transaction> get _recentTransactions{
    return _userTransactions.where((element) {
      return element.date.isAfter(DateTime.now().subtract(const Duration(days: 7)));
    }).toList();
  }


  void addNewTransaction(String txTitle,double txAmount,DateTime txDate){
    final newTx = Transaction(
        id:DateTime.now().toString(),
        title:txTitle,
        amount:txAmount,
        date:txDate,
    );
    setState(() {
      _userTransactions.add(newTx);
    });
  }

  void deleteTransaction(String id){
    setState(() {
          _userTransactions.removeWhere((element) => element.id==id);
    });
  }
  void addNewTransactionField(BuildContext ctx){
    showModalBottomSheet(
        context: ctx,
        builder: (_){
          return NewTransaction(addTx: addNewTransaction);
        },
    );
  }

  @override
  Widget build(BuildContext context) {
    final bool isLandscape = MediaQuery.of(context).orientation == Orientation.landscape;
    final appBar = AppBar(
      title: const Text(
        "Personal Expenses",
      ),
      actions: [
        IconButton(
          onPressed: ()=>addNewTransactionField(context),
          icon: const Icon(Icons.add),
        )
      ],
    );
    final txListWidget = SizedBox(
        height: (MediaQuery.of(context).size.height-
            appBar.preferredSize.height-
            MediaQuery.of(context).padding.top)*0.7,
        child: TransactionList(transactions:_userTransactions,deleteTxt: deleteTransaction,));
    return Scaffold(
      appBar:  appBar,
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children:  [
            if (isLandscape) Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Show Chart"),
                Switch(
                    value: _showChart,
                    onChanged: (val){
                        setState(() {
                          _showChart = val;

                        });
                    } ,
                ),
              ],
            ),
            if(!isLandscape) SizedBox(
                height: (MediaQuery.of(context).size.height-
                    appBar.preferredSize.height -
                    MediaQuery.of(context).padding.top)*0.3 ,
                child: Chart(recentTransactions: _recentTransactions)),
            if(!isLandscape) txListWidget,
            if(isLandscape)
            _showChart ? SizedBox(
              height: (MediaQuery.of(context).size.height-
                  appBar.preferredSize.height -
                  MediaQuery.of(context).padding.top)*0.7 ,
                child: Chart(recentTransactions: _recentTransactions)) :
                txListWidget,
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        onPressed:()=>addNewTransactionField(context),
        child: const Icon(Icons.add),
      ),
    );
  }
}

