import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NewTransaction extends StatefulWidget {
  final Function addTx;
  const NewTransaction({Key? key,required this.addTx}) : super(key: key);

  @override
  State<NewTransaction> createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final titleController = TextEditingController();

  final amountController = TextEditingController();
  DateTime? selectedDate;
  void submitData(){
    if (amountController.text.isEmpty){
      return;
    }
    final enteredTitle = titleController.text;
    final enteredAmount = double.parse(amountController.text);

    if(enteredTitle.isEmpty || enteredAmount<=0 || selectedDate == null){
      return;
    }
    widget.addTx(enteredTitle,enteredAmount,selectedDate);
    Navigator.of(context).pop();
  }

  void presentDayPicker() async{
    final DateTime? selected = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2018),
        lastDate: DateTime.now()
    );
    setState(() {
      selectedDate = selected;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Card(
        child: Container(
          padding: EdgeInsets.only(
              top: 10,
              left: 10,
              right: 10,
              bottom: MediaQuery.of(context).viewInsets.bottom + 10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TextField(
                decoration: const InputDecoration(
                    labelText: 'Title'
                ),
                controller: titleController,
                  onSubmitted: (_)=>submitData(),
              ),
              TextField(
                decoration: const InputDecoration(
                    labelText: 'Amount'
                ),
                controller: amountController,
                keyboardType: TextInputType.number,
                onSubmitted: (_)=>submitData(),
              ),
              SizedBox(
                height: 70,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                          selectedDate==null ?
                          "No Date Chosen!!" :
                          "Selected Date ${DateFormat.yMd().format(selectedDate!)}",
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                      ),
                    ),
                    TextButton(
                      style: ButtonStyle(
                        foregroundColor: MaterialStateProperty.all<Color>(
                            Theme.of(context).primaryColor
                        )
                      ),
                        onPressed: presentDayPicker,
                        child: const Text("Choose date")
                    ),
                  ],
                ),
              ),
              ElevatedButton(
                onPressed: submitData,
                child: const Text("Add Transaction"),
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                    Theme.of(context).primaryColor
                  ),
                  foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
