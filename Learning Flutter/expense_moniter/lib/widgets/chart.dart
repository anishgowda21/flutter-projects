import 'package:expense_moniter/models/transaction.dart';
import 'package:expense_moniter/widgets/chart_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
class Chart extends StatelessWidget {
  
  const Chart({Key? key,required this.recentTransactions}) : super(key: key);
  
  final List<Transaction> recentTransactions;
  List<Map<String,dynamic>> get groupedTransactionValue {
    return List.generate(7, (index) {
      final weekDay = DateTime.now().subtract(Duration(days: index));
      var totalSum=0.0;
      for(var i=0; i< recentTransactions.length;i++){
        if(recentTransactions[i].date.day == weekDay.day &&
        recentTransactions[i].date.month == weekDay.month&&
        recentTransactions[i].date.year == weekDay.year){
          totalSum += recentTransactions[i].amount;
        }
      }
      //debugPrint(DateFormat.E().format(weekDay).toString());
      //debugPrint(totalSum.toString());
      //debugPrint("\n");

      return {'day' : DateFormat.E().format(weekDay), 'amount':totalSum};
    }).reversed.toList();
  }

  double get maxSpending {
    return groupedTransactionValue.fold(0.0, (sum, element) {
      return sum+element['amount'];
    });
  }
  @override
  Widget build(BuildContext context) {
    debugPrint(groupedTransactionValue.toString());
    return Card(
          elevation: 6,
          margin: const EdgeInsets.all(20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: groupedTransactionValue.map((e) {
              return Flexible(
                fit: FlexFit.tight,
                child: ChartBar(
                    label: e['day'],
                    spendingAmount: e['amount'],
                    spendingPctOfTotal: maxSpending==0.0? 0.0 : e['amount']/maxSpending),
              );
            }).toList(),
          ),
        );
  }
}
