import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/transaction.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deleteTxt;
  const TransactionList(
      {Key? key,
        required this.transactions,
        required this.deleteTxt
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return transactions.isEmpty ? LayoutBuilder(
        builder: (ctx,constraints){
          return Column(
            children: [
              Text(
                'No Transactions added yet!!',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const SizedBox(height: 10,),
              SizedBox(
                  height: constraints.maxHeight*0.6,
                  child: Image.asset(
                    'assets/images/waiting.png',
                    fit: BoxFit.cover,
                  )
              ),
            ],
          );
        }
    ) : ListView.builder(
        itemCount: transactions.length,
        itemBuilder: (ctx,index){
          return Card(
            elevation: 6,
            margin: const EdgeInsets.symmetric(vertical: 8,horizontal: 5),
            child: ListTile(
              leading: CircleAvatar(
                radius: 30,
                child: Padding(
                  padding: const EdgeInsets.all(6),
                  child: FittedBox(
                      child: Text("\$${transactions[index].amount}")),
                ),
              ),
              title: Text(
                  transactions[index].title,
                style: Theme.of(context).textTheme.titleLarge,
              ),
              subtitle: Text(
                DateFormat.yMMMd().format(transactions[index].date),
              ),
              trailing: IconButton(
                icon: Icon(
                    Icons.delete,
                  color: Theme.of(context).errorColor,
                ),
                onPressed: ()=>deleteTxt(transactions[index].id),
              ),

            ),
          );
        }
      );
  }
}

